package com.example.profedam.listviewsample;

import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.profedam.listviewsample.logic.Element;

import java.util.ArrayList;

public class ElementAdapter extends BaseAdapter {

    private ArrayList<Element> llistaElements;
    private Context context;

    public ElementAdapter(ArrayList<Element> llistaElements, Context context) {
        this.llistaElements = llistaElements;
        this.context = context;
    }

    @Override
    public int getCount() {
        return llistaElements.size();
    }

    @Override
    public Object getItem(int i) {
        return llistaElements.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = view;
        System.out.println("Hola");
        Log.i("getView", "getView");
        if (rowView==null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.row_layout, viewGroup, false);
        }
            TextView tvRow = rowView.findViewById(R.id.tvRow);
            tvRow.setText(llistaElements.get(i).getTitol());
            ImageView imgRow = rowView.findViewById(R.id.imageView);
            imgRow.setImageResource(llistaElements.get(i).getFoto());

        return rowView;

    }
}
