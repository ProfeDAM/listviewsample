package com.example.profedam.listviewsample.logic;

public class Element {

   private String titol;
   private  int foto;

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public Element(String titol, int foto) {
        this.titol = titol;
        this.foto = foto;
    }
}
