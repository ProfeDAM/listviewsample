package com.example.profedam.listviewsample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.profedam.listviewsample.logic.Element;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Element> llistaElements;
    private String[] sistemes = {"Windows", "Ubuntu 19.04", "Ios 13", "Android"};
    private int[] icons = {R.drawable.windows, R.drawable.ubuntu1904, R.drawable.ios13,R.drawable.android};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView lvLlista = findViewById(R.id.lvLlista);
        llistaElements = new ArrayList();
        for (int i=0; i<sistemes.length; i++ )
        {
          llistaElements.add (new Element(sistemes[i], icons[i]));
        }
        Toast.makeText(this, llistaElements.size()+"", Toast.LENGTH_LONG).show();
        ElementAdapter elementAdapter = new ElementAdapter(llistaElements, this);
        lvLlista.setAdapter(elementAdapter);
        elementAdapter.notifyDataSetChanged();

    }
}
